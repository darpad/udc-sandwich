package com.udacity.sandwichclub.utils;

import com.udacity.sandwichclub.model.Sandwich;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class JsonUtils {
    private static final String DESCRIPTION = "description";
    private static final String IMAGE = "image";
    private static final String PLACE_OF_ORIGIN = "placeOfOrigin";
    private static final String NAME = "name";
    private static final String MAIN_NAME = "mainName";
    private static final String ALSO_KNOWN_AS = "alsoKnownAs";
    public static final String INGREDIENTS = "ingredients";


    public static Sandwich parseSandwichJson(String json) {
        final Sandwich sandwich = new Sandwich();
        try {
            final JSONObject rootObj = new JSONObject(json);
            readFromNameObj(sandwich, rootObj);
            readFromRootObj(sandwich, rootObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return sandwich;
    }

    private static void readFromRootObj(Sandwich sandwich, JSONObject rootObj) throws JSONException {
        sandwich.setDescription(rootObj.getString(DESCRIPTION));
        sandwich.setImage(rootObj.getString(IMAGE));
        sandwich.setPlaceOfOrigin(rootObj.getString(PLACE_OF_ORIGIN));
        sandwich.setIngredients(readIngredients(rootObj));
    }

    private static List<String> readIngredients(JSONObject rootObj) throws JSONException {
        JSONArray jsonArray = rootObj.getJSONArray(INGREDIENTS);
        return convertJsonArrayToList(jsonArray);
    }

    private static void readFromNameObj(Sandwich sandwich, JSONObject jsonObj) throws JSONException {
        JSONObject nameObj = jsonObj.getJSONObject(NAME);
        sandwich.setAlsoKnownAs(readAlsoKnows(nameObj));
        sandwich.setMainName(nameObj.getString(MAIN_NAME));
    }

    private static List<String> readAlsoKnows(JSONObject nameObj) throws JSONException {
        JSONArray alsoKnowAs = nameObj.getJSONArray(ALSO_KNOWN_AS);
        return convertJsonArrayToList(alsoKnowAs);
    }

    private static List<String> convertJsonArrayToList(JSONArray jsonArray) throws JSONException {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            list.add(jsonArray.getString(i));
        }

        return list;
    }

}
